# définition et variables (les valeurs sont dans le fichier terraform.tfvars)

variable "aws_region" {
    description = "AWS Region"
    type = string
}

variable "bucket_name" {
    description = "Name of the Bucket"
    type = string
}