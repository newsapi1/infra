terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

# definition de la région par une variable dans terraform.tfvars et variable.tf
provider "aws" {
    region = var.aws_region
}



# création instance preprod
resource "aws_instance" "frontend_preprod" {
  ami           = "ami-087da76081e7685da"
  instance_type = "t2.micro"
  key_name   = "aws_projet"
  vpc_security_group_ids = [aws_security_group.frontend-security-group.id]
  tags = {
    Name = "terraform-frontend"
  }
}
# assignation cle ssh
resource "aws_key_pair" "aws_projet" {
  key_name   = "aws_projet"
  public_key = file("aws_projet.pub")
}
# création groupe de securité backend
resource "aws_security_group" "frontend-security-group" {
  name        = "frontend-security-group"
  description = "Allow SSH, HTTP, and HTTPS"
  vpc_id    = "vpc-0019dcb25930dab75"
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
}
 ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
 
# création instance pour le backend
resource "aws_instance" "backend_preprod" {
  ami           = "ami-087da76081e7685da"
  instance_type = "t2.micro"
  key_name = "aws_projet"
  vpc_security_group_ids = [aws_security_group.backend-security-group.id]
  tags = {
    Name = "terraform-backend"
  }
  
}

# création groupe de sécurité backend
resource "aws_security_group" "backend-security-group" {
  name        = "backend-security-group"
  description = "Allow SSH, HTTP, and HTTPS"
  vpc_id    = "vpc-0019dcb25930dab75"
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
}
 ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

# module qui copie les fichiers dans le bucket lors du lancement 
# de terraform/création du bucket
module "template_files" {
    source = "hashicorp/dir/template"

    base_dir = "${path.module}/frontend"
}
# création bucket
resource "aws_s3_bucket" "hosting_bucket" {
    bucket = var.bucket_name
}
# droits par défaut: tous les fichierscrées dans le bucket appartiennent au bucket 
# (et pas au compte aws qui envoi les fichiers)
resource "aws_s3_bucket_ownership_controls" "hosting_bucket" {
  bucket = aws_s3_bucket.hosting_bucket.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}
# bucket en acces publique
resource "aws_s3_bucket_acl" "hosting_bucket" {
  depends_on = [
    aws_s3_bucket_ownership_controls.hosting_bucket,
    aws_s3_bucket_public_access_block.hosting_bucket,
  ]

  bucket = aws_s3_bucket.hosting_bucket.id
  acl    = "public-read"
}

# page d'accueil par défaut: obligatoire pour la création d'un bucket qui heberge un site statique
resource "aws_s3_bucket_website_configuration" "hosting_bucket_website_configuration" {
  bucket = aws_s3_bucket.hosting_bucket.id

  index_document {
    suffix = "index.html"
  }
}

# désactivation des blocages d'acces public
resource "aws_s3_bucket_public_access_block" "hosting_bucket" {
  bucket = aws_s3_bucket.hosting_bucket.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

# copie tous les fichiers dans le dossier paramétrés dans le module "template_files"
resource "aws_s3_object" "hosting_bucket_files" {
    bucket = aws_s3_bucket.hosting_bucket.id

    for_each = module.template_files.files

    key = each.key
    content_type = each.value.content_type

    source = each.value.source_path
    content = each.value.content

    etag = each.value.digests.md5
}


